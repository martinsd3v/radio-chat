package com.playground.marcelo.radiochat;

/**
 * Created by marcelo on 28/09/16.
 */

public class MensagemData {
    //Informações do remetente quem esta enviando a mensagem
    public String remetente_nome;
    public String remetente_foto;
    public String remetente_id;

    //Identificador usuário app
    public String usuario_app_id;

    //App ou radio
    public String origem;

    //Informaçoes de mensagem
    public String mensagem_data;
    public String mensagem_texto;
    public String mensagem_tipo;
    public String mensagem_arquivo_status;
    public String mensagem_arquivo_web;
    public String mensagem_arquivo_local;
    public String mensagem_arquivo_server;
    public String registro_id;

    public MensagemData() {

    }

    public String getRemetente_nome() {
        return remetente_nome;
    }

    public void setRemetente_nome(String remetente_nome) {
        this.remetente_nome = remetente_nome;
    }

    public String getRemetente_foto() {
        return remetente_foto;
    }

    public void setRemetente_foto(String remetente_foto) {
        this.remetente_foto = remetente_foto;
    }

    public String getRemetente_id() {
        return remetente_id;
    }

    public void setRemetente_id(String remetente_id) {
        this.remetente_id = remetente_id;
    }

    public String getUsuario_app_id() {
        return usuario_app_id;
    }

    public void setUsuario_app_id(String usuario_app_id) {
        this.usuario_app_id = usuario_app_id;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getMensagem_data() {
        return mensagem_data;
    }

    public void setMensagem_data(String mensagem_data) {
        this.mensagem_data = mensagem_data;
    }

    public String getMensagem_texto() {
        return mensagem_texto;
    }

    public void setMensagem_texto(String mensagem_texto) {
        this.mensagem_texto = mensagem_texto;
    }

    public String getMensagem_arquivo_status() {
        return mensagem_arquivo_status;
    }

    public void setMensagem_arquivo_status(String mensagem_arquivo_status) {
        this.mensagem_arquivo_status = mensagem_arquivo_status;
    }

    public String getMensagem_arquivo_web() {
        return mensagem_arquivo_web;
    }

    public void setMensagem_arquivo_web(String mensagem_arquivo_web) {
        this.mensagem_arquivo_web = mensagem_arquivo_web;
    }

    public String getMensagem_arquivo_local() {
        return mensagem_arquivo_local;
    }

    public void setMensagem_arquivo_local(String mensagem_arquivo_local) {
        this.mensagem_arquivo_local = mensagem_arquivo_local;
    }

    public String getMensagem_tipo() {
        return mensagem_tipo;
    }

    public void setMensagem_tipo(String mensagem_tipo) {
        this.mensagem_tipo = mensagem_tipo;
    }

    public String getRegistro_id() {
        return registro_id;
    }

    public void setRegistro_id(String registro_id) {
        this.registro_id = registro_id;
    }

    public String getMensagem_arquivo_server() {
        return mensagem_arquivo_server;
    }

    public void setMensagem_arquivo_server(String mensagem_arquivo_server) {
        this.mensagem_arquivo_server = mensagem_arquivo_server;
    }
}
