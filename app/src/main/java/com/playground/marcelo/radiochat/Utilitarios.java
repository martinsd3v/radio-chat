package com.playground.marcelo.radiochat;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by marcelo on 28/09/16.
 */

public class Utilitarios {
    //Retorna a data atual já no padrão
    public String dataAtual() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM HH:mm");
        return df.format(c.getTime());
    }

    //Retorna a data atual no formato para salvar arquivos
    public String fileData() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return df.format(c.getTime());
    }

    //Cria um diretorio
    public void criaDiretorio(String diretorio) {
        File file = new File(diretorio);
        if (!file.isDirectory()) file.mkdirs();
        System.out.println("LOG: " + diretorio);
    }

    //Apaga arquivo em determinada pasta
    public void apagaArquivo(String arquivo) {
        if (arquivo != null) {
            File file = new File(arquivo);
            if (file.exists()) file.delete();
        }
    }
}
