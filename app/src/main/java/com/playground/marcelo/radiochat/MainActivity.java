package com.playground.marcelo.radiochat;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.webkit.MimeTypeMap;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.playground.marcelo.radiochat.databinding.MyChatBinding;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;//Instancia auth
    private FirebaseDatabase database;//Instancia database
    private DatabaseReference databaseReference;//Instancia da referencia
    private Query queryRef;//Query de referencia

    private MyChatBinding binding;//Instancia Databinding
    private Intent storageService;//Serviço de envio de arquivos
    private String userUid = null;//Usuario logado
    private List<MensagemData> mList = new ArrayList<>();//Lista de mensagens

    //Informações do usuario atual
    private String usuario_app_id = "16";

    private Utilitarios utilitarios = new Utilitarios();

    //Variavel Global para recuperar se existe texto
    private boolean isTexto = false;
    private boolean isLoad = false;

    //Trabalhando com arquivos
    private String selectedImagePath;

    //Especificacoes de midias
    private String midiaPath;
    private String midiaName;
    private File file;
    private Uri outputFileUri;

    CountDownTimer timeAudio;

    private final int TYPE_PHOTO = 1;
    private final int TYPE_VIDEO = 2;
    private final int TYPE_FILE = 3;
    private final int TYPE_AUDIO = 4;

    private MediaRecorder mRecorder = null;
    private Vibrator vibrator;
    private long inicioRecorder;

    private boolean altorizacao = false;
    String[] permissoes = new String[]{
            android.Manifest.permission.RECORD_AUDIO,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
            android.Manifest.permission.CAMERA,
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Resetando variaveis globais
        isTexto = false;
        if (mRecorder != null) mRecorder.reset();
        mRecorder = null;


        //Iniciando databinding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        //Preparando autenticação
        auth = FirebaseAuth.getInstance();
        //Preparando base de dados
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("MensagensRadio");
        //Pedindo permissoes ao usuario
        altorizacao = PermissionUtils.validate(this, 0, permissoes);
        //Inicializando servico vibrator
        vibrator = (Vibrator) this.getSystemService(VIBRATOR_SERVICE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.resultadoId.scrollToPosition(mList.size() - 1);
        auth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) { //Login efetuado com sucesso
                    FirebaseUser usuario = auth.getCurrentUser();
                    if (usuario != null) {
                        userUid = usuario.getUid();
                        iniciaApp();
                    }
                }
            }
        });

        binding.resultadoId.addOnItemTouchListener(new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, float x, float y) {
                if (mList.get(position) != null) {
                    MensagemData mensagemData = mList.get(position);
                    if (!mensagemData.mensagem_tipo.equals("texto") && mensagemData.mensagem_arquivo_local != null) {

                        String midiaPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MidiasRadio";
                        midiaPath += File.separator + mensagemData.mensagem_tipo + File.separator;

                        File file = new File(midiaPath + mensagemData.mensagem_arquivo_local);
                        if (file.exists()) {
                            String extension = MimeTypeMap.getFileExtensionFromUrl(mensagemData.mensagem_arquivo_local);
                            if (extension != null) {
                                String type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
                                Intent intent = new Intent();
                                intent.setAction(Intent.ACTION_VIEW);
                                intent.setDataAndType(Uri.fromFile(file), type);
                                startActivity(intent);
                            }
                        } else {
                            if (mensagemData.mensagem_arquivo_status.equals("Concluido"))
                                baixarArquivo(mensagemData.registro_id);
                            else
                                aguardandoEnvio();
                        }
                    }
                }
                System.out.println("LOG: cliclou no item:" + position);
            }
        }));
    }

    private void iniciaApp() {
        //Clicando em enviar mensagem
        binding.enviarId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isTexto) enviaMensagem();
            }
        });

        //Clicando em capturar imagem
        binding.midiaId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                capturaMidia();
            }
        });

        timeAudio = new CountDownTimer(Long.MAX_VALUE, 1000) {
            long cnt = 0;

            @Override
            public void onTick(long millisUntilFinished) {
                cnt++;
                String time = new Integer((int) cnt).toString();

                long millis = cnt;
                int seconds = (int) (millis / 60);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                binding.timeAudioId.setText(String.format("%02d:%02d:%02d", minutes, seconds, millis));
            }

            @Override
            public void onFinish() {
                cnt = 0;
                binding.timeAudioId.setText("00:00:00");
            }
        };

        capturaAudio();
        alimentaRv();
        alteraBotoes();


    }

    //Metodo responsavel por efetuar captura de audio
    public void capturaAudio() {
        binding.gravarId.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!isTexto) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            if (checkPermission() && checkInternet()) {
                                if (mRecorder != null) mRecorder.reset();
                                try {
                                    binding.audioMsgId.setVisibility(View.VISIBLE);
                                    binding.mensagemId.setVisibility(View.GONE);
                                    binding.limitadorId.setVisibility(View.GONE);
                                    binding.midiaId.setVisibility(View.GONE);

                                    //Setando diretorio do arquivo
                                    prepareMidia(TYPE_AUDIO).toString();
                                    //Parametros de configuração do audio
                                    mRecorder = new MediaRecorder();
                                    mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                                    mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                                    mRecorder.setOutputFile(midiaPath + midiaName);
                                    mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

                                    //Inicia a gravação do audio
                                    mRecorder.prepare();
                                    vibrator.vibrate(80);
                                    Calendar c = Calendar.getInstance();
                                    inicioRecorder = c.getTimeInMillis();
                                    mRecorder.start();
                                    timeAudio.start();

                                } catch (IOException e) {
                                    mRecorder = null;
                                    e.printStackTrace();
                                }
                            }

                            break;
                        case MotionEvent.ACTION_UP:
                            try {
                                binding.audioMsgId.setVisibility(View.GONE);
                                binding.mensagemId.setVisibility(View.VISIBLE);
                                binding.limitadorId.setVisibility(View.VISIBLE);
                                binding.midiaId.setVisibility(View.VISIBLE);

                                timeAudio.cancel();
                                timeAudio.onFinish();

                                //Finaliza a gravação
                                mRecorder.stop();
                                mRecorder.release();
                                mRecorder = null;
                                vibrator.vibrate(80);
                                Calendar c = Calendar.getInstance();
                                if (c.getTimeInMillis() - inicioRecorder < 500 || !checkInternet())
                                    utilitarios.apagaArquivo(midiaPath + midiaName);//Apagar audio se menor que 1/2 segundo
                                else
                                    confirmaEnvioAudio();
                            } catch (RuntimeException e) {
                                utilitarios.apagaArquivo(midiaPath + midiaName);//Apaga audio se der erro
                                e.printStackTrace();
                            }
                            break;
                    }
                }
                return false;
            }
        });
    }

    //Metodo responsavel por inicializar camera
    public void capturaMidia() {
        final CharSequence[] items = {"Tirar foto agora.", "Gravar um video.", "Selecionar na galeria."};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Como deseja enviar a midia?");
        builder.setCancelable(true);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        if (checkPermission() && checkInternet()) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, prepareMidia(TYPE_PHOTO));
                            startActivityForResult(intent, TYPE_PHOTO);
                        }
                        break;
                    case 1:
                        if (checkPermission() && checkInternet()) {
                            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, prepareMidia(TYPE_VIDEO));
                            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
                            startActivityForResult(intent, TYPE_VIDEO);
                        }
                        break;
                    case 2:
                        if (checkInternet()) {
                            Intent intent = new Intent();
                            intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/* video/*");
                            startActivityForResult(intent, TYPE_FILE);
                        }
                        break;
                    default:
                        dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    //Pegando resultando da intent e enviando o arquivo
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("LOG: mostrando " + requestCode);

        if (resultCode == RESULT_OK) {
            Uri selectedMidiaUri;
            switch (requestCode) {
                case TYPE_FILE:
                    selectedMidiaUri = data.getData();
                    selectedImagePath = getPath(selectedMidiaUri);
                    sendMidia(selectedImagePath, "midia");
                    break;
                case TYPE_VIDEO:
                    selectedImagePath = getPath(outputFileUri);
                    sendMidia(selectedImagePath, "video");
                    break;
                case TYPE_PHOTO:
                    selectedImagePath = getPath(outputFileUri);
                    sendMidia(selectedImagePath, "foto");
                    break;
                default:
                    System.out.println("LOG: " + requestCode);
            }
        }
    }

    //Metodo responsavel por exibir modal de download
    public void baixarArquivo(final String identificador) {
        final CharSequence[] items = {"Sim, quero baixar.", "Não, foi sem querer."};

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Deseja baixar este arquivo?");
        builder.setCancelable(false);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        storageService = new Intent(MainActivity.this, StorageServiceDownload.class);
                        storageService.putExtra("identificador", identificador);
                        startService(storageService);
                        System.out.println("LOG: servico chamado");
                        dialog.dismiss();
                        break;
                    default:
                        dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    //Metodo responsavel por exibir modal com status do envio
    public void aguardandoEnvio() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Também estamos aguardando...");
        builder.setMessage("Aguarde o envio ser concluido");
        builder.setCancelable(true);
        builder.show();
    }

    //Metodo responsavel por exibir a modal de confirmação
    public void confirmaEnvioAudio() {
        final CharSequence[] items = {"Sim, pode enviar.", "Não, foi sem querer."};

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Deseja enviar o audio gravado?");
        builder.setCancelable(false);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        sendMidia(midiaPath + midiaName, "audio");
                        break;
                    case 1:
                        utilitarios.apagaArquivo(midiaPath + midiaName);
                        break;
                    default:
                        dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    //Altera os botões de enviar audio mensagem foto e video
    public void alteraBotoes() {
        final Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setDuration(1000);

        final Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setDuration(500);

        //verifica alterações no editText
        binding.mensagemId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    if (!isTexto) {
                        binding.midiaId.startAnimation(fadeOut);
                        binding.midiaId.setVisibility(View.GONE);
                        binding.gravarId.startAnimation(fadeOut);
                        binding.gravarId.setVisibility(View.GONE);
                        binding.enviarId.startAnimation(fadeIn);
                        binding.enviarId.setVisibility(View.VISIBLE);
                    }
                    isTexto = true;
                } else {
                    if (isTexto) {
                        binding.enviarId.startAnimation(fadeOut);
                        binding.enviarId.setVisibility(View.GONE);
                        binding.midiaId.startAnimation(fadeIn);
                        binding.midiaId.setVisibility(View.VISIBLE);
                        binding.gravarId.startAnimation(fadeIn);
                        binding.gravarId.setVisibility(View.VISIBLE);
                    }
                    isTexto = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //Preparando arquivo e diretorio para salvar o arquivo
    private Uri prepareMidia(int type) {
        //Diretorio padrão
        midiaPath = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "MidiasRadio";
        midiaName = null;
        switch (type) {
            case TYPE_VIDEO:
                midiaPath += File.separator + "video" + File.separator;
                midiaName = "VID_" + utilitarios.fileData() + ".mp4";
                utilitarios.criaDiretorio(midiaPath);
                file = new File(midiaPath + midiaName);
                break;
            case TYPE_PHOTO:
                midiaPath += File.separator + "foto" + File.separator;
                midiaName = "PHT_" + utilitarios.fileData() + ".jpg";
                utilitarios.criaDiretorio(midiaPath);
                file = new File(midiaPath + midiaName);
                break;
            case TYPE_AUDIO:
                midiaPath += File.separator + "audio" + File.separator;
                midiaName = "AUD_" + utilitarios.fileData() + ".mp3";
                utilitarios.criaDiretorio(midiaPath);
                file = new File(midiaPath + midiaName);
                break;
            default:
                return null;
        }
        outputFileUri = Uri.fromFile(file);
        return outputFileUri;
    }

    //Envia midias para o serviço de upload
    private void sendMidia(String arquivo, String tipo) {
        if (arquivo != null) {

            String filename = arquivo.substring(arquivo.lastIndexOf("/") + 1);

            MensagemData mensagemData = new MensagemData();
            mensagemData.setMensagem_data(utilitarios.dataAtual());
            mensagemData.setUsuario_app_id(usuario_app_id);
            mensagemData.setMensagem_arquivo_local(filename);
            mensagemData.setMensagem_tipo(tipo);
            mensagemData.setOrigem("app");
            mensagemData.setMensagem_texto(" ");
            mensagemData.setMensagem_arquivo_status("aguardando");

            String mensagemKey = databaseReference.push().getKey();
            databaseReference.child(mensagemKey).setValue(mensagemData);
            incrementaQtd();

            storageService = new Intent(MainActivity.this, StorageServiceUpload.class);
            storageService.putExtra("arquivo", arquivo);
            storageService.putExtra("identificador", mensagemKey);
            storageService.putExtra("usuario", userUid);
            storageService.putExtra("referencia", tipo);
            startService(storageService);
        }
    }

    //Pega diretorio completo de um determinado arquivo
    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    //Retorna se usuário tem conexão
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    //Verifica se usuário tem conexão com internet
    private boolean checkInternet() {
        if (!isOnline()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Você esta sem conexão!");
            builder.setMessage("É necessário ter conexão com internet.");
            AlertDialog dialog = builder.create();
            dialog.show();
            return false;
        } else {
            return true;
        }
    }

    //Verficia se foi dando permissões necessárias
    private boolean checkPermission() {
        //Se não tiver permissão parar execucão
        if (!altorizacao) {
            System.out.println("LOG: Entrou!");

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("App sem permissões.");
            builder.setMessage("Você não deu as permissoes requeridas!");
            AlertDialog dialog = builder.create();
            dialog.show();

            return false;
        } else {
            return true;
        }
    }

    private void incrementaQtd() {
        //Verifica se usuario existe e adiciona mais um na contagem
        final DatabaseReference usuarioReference = database.getReference("UsuariosRadio");
        usuarioReference.child(usuario_app_id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Calendar c = Calendar.getInstance();
                    String qtd_msg = (String) dataSnapshot.child("qtd_msg").getValue();
                    Integer quantidade = Integer.parseInt(qtd_msg) + 1;
                    usuarioReference.child(dataSnapshot.getKey()).child("qtd_msg").setValue(String.valueOf(quantidade));
                    usuarioReference.child(dataSnapshot.getKey()).child("interacao_time").setValue(String.valueOf(c.getTime()));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Cadastra mensagem no firebase
    private void enviaMensagem() {
        if (checkInternet()) {
            String mensagem = String.valueOf(binding.mensagemId.getText());
            if (!mensagem.equals("")) {
                //Se realmente tiver mensagem então cadastrar
                MensagemData mensagemData = new MensagemData();
                mensagemData.setMensagem_texto(mensagem);
                mensagemData.setMensagem_data(utilitarios.dataAtual());
                mensagemData.setUsuario_app_id(usuario_app_id);
                mensagemData.setOrigem("app");
                mensagemData.setMensagem_tipo("texto");
                mensagemData.setMensagem_arquivo_status("Concluido");
                databaseReference.push().setValue(mensagemData);
                binding.mensagemId.setText("");//Cadastra mensagem

                incrementaQtd();
            }
        }
    }

    //Recupera mensagens do firebase
    private void alimentaRv() {
        if (queryRef == null) {
            String key = "usuario_app_id";
            queryRef = databaseReference.orderByChild(key).equalTo(usuario_app_id);

            //Setando layout do recycliviwe
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            binding.resultadoId.setLayoutManager(linearLayoutManager);

            queryRef.addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String previousChild) {
                    montaLista(dataSnapshot);
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                    montaLista(dataSnapshot);
                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    //Metodo responsavel por montar lista no recyclieview
    private void montaLista(DataSnapshot dataSnapshot) {
        if (!isLoad) {
            final Animation fadeIn = new AlphaAnimation(0, 1);
            fadeIn.setDuration(1000);

            final Animation fadeOut = new AlphaAnimation(1, 0);
            fadeOut.setDuration(500);

            isLoad = true;
            binding.aguardeMsgId.startAnimation(fadeOut);
            binding.aguardeMsgId.setVisibility(View.GONE);
            binding.resultadoId.startAnimation(fadeIn);
            binding.resultadoId.setVisibility(View.VISIBLE);
        }

        MensagemData mensagemData = dataSnapshot.getValue(MensagemData.class);
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).registro_id.equals(dataSnapshot.getKey())) mList.remove(i);
        }

        mensagemData.setRegistro_id(dataSnapshot.getKey());
        mList.add(mensagemData);
        MensagemAdapter mensagemAdapter = new MensagemAdapter(mList);
        binding.resultadoId.scrollToPosition(mList.size() - 1);
        binding.resultadoId.setAdapter(mensagemAdapter);
    }

    @Override
    protected void onDestroy() {
        if (storageService != null) stopService(storageService);
        super.onDestroy();
    }

    //Interseptando alteração em permissões
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int result : grantResults) {
            if (result == PackageManager.PERMISSION_DENIED) {
                // Alguma permissão foi negada
                altorizacao = false;
                return;
            }
        }
        //Permissoes OK
        altorizacao = true;
    }
}
